const mysql = require("mysql");


//Configuracion de la base de datos

const mysqlConnection = mysql.createConnection({
  host: "localhost",
  port: 3306,
  user: "root",
  password: "admin01",
  database: "latablita"
});

//Conexion a la base de datos

mysqlConnection.connect((err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("Conexion exitosa");
  }
});

//Modulo a exportar

module.exports = mysqlConnection;
