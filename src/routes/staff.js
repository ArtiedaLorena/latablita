const express = require("express");

const router = express.Router();

const mysqlConnection = require("../database/connection");

router.get("/", (req, res) => {
  mysqlConnection.query("SELECT * FROM staff", (err, rows, fields) => {
    if (err) {
      console.log(err);
    } else {
      res.json(rows);
    }
  });
});

router.get("/:id", (req, res) => {
  mysqlConnection.query(
    "SELECT * FROM staff WHERE id= ?",
    [req.params.id],
    (err, rows, fields) => {
      if (err) {
        console.log(err);
      } else {
        res.json(rows[0]);
      }
    }
  );
});

router.post("/", (req, res) => {
  const { id, name, store_id, position_id } = req.body;

  const query = "CALL addOrEditStaff(?,?,?,?)";

  mysqlConnection.query(
    query,
    [id, name, store_id, position_id],
    (err, rows, fields) => {
      if (!err) {
        res.json({ status: "Empleado añadido exitosamente!" });
      } else {
        console.log(err);
      }
    }
  );
});

router.put("/:id", (req, res) => {
  const { id } = req.params;
  const { name, store_id, position_id } = req.body;

  const query = "CALL addOrEditStaff(?,?,?,?)";
  mysqlConnection.query(
    query,
    [id, name, store_id, position_id],
    (err, rows, fields) => {
      if (!err) {
        res.json({ status: "Empleado editado exitosamente!" });
      } else {
        console.log(err);
      }
    }
  );
});

router.delete("/:id", (req, res) => {
  const { id } = req.params;

  mysqlConnection.query(
    "DELETE FROM staff WHERE id = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        res.json({ status: "Empleado eliminado con exito!" });
      } else {
        console.log(err);
      }
    }
  );
});

module.exports = router;
