const express = require("express");

const router = express.Router();

const mysqlConnection = require("../database/connection");

router.get("/", (req, res) => {
  mysqlConnection.query("SELECT * FROM stores", (err, rows, fields) => {
    if (err) {
      console.log(err);
    } else {
      res.json(rows);
    }
  });
});

router.get("/:id", (req, res) => {
  mysqlConnection.query(
    "SELECT * FROM stores WHERE id= ?",
    [req.params.id],
    (err, rows, fields) => {
      if (err) {
        console.log(err);
      } else {
        res.json(rows[0]);
      }
    }
  );
});

router.post("/", (req, res) => {
  const { id, name, city } = req.body;

  const query = "CALL addOrEditStores(?,?,?)";

  mysqlConnection.query(query, [id, name, city], (err, rows, fields) => {
    if (!err) {
      res.json({ status: "Sucursal añadida exitosamente!" });
    } else {
      console.log(err);
    }
  });
});

router.put("/:id", (req, res) => {
  const { id } = req.params;
  const { name, city } = req.body;

  const query = "CALL addOrEditStores(?,?,?)";
  mysqlConnection.query(query, [id, name, city], (err, rows, fields) => {
    if (!err) {
      res.json({ status: "Sucursal editada exitosamente!" });
    } else {
      console.log(err);
    }
  });
});

router.delete("/:id", (req, res) => {
  const { id } = req.params;

  mysqlConnection.query(
    "DELETE FROM stores WHERE id = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        res.json({ status: "Sucursal eliminada con exito!" });
      } else {
        console.log(err);
      }
    }
  );
});

module.exports = router;
