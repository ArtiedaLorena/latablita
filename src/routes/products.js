const express = require("express");

const router = express.Router();

const mysqlConnection = require("../database/connection");

router.get("/", (req, res) => {
  mysqlConnection.query("SELECT * FROM products", (err, rows, fields) => {
    if (err) {
      console.log(err);
    } else {
      res.json(rows);
    }
  });
});

router.get("/:id", (req, res) => {
  mysqlConnection.query(
    "SELECT * FROM products WHERE id= ?",
    [req.params.id],
    (err, rows, fields) => {
      if (err) {
        console.log(err);
      } else {
        res.json(rows[0]);
      }
    }
  );
});

router.post("/", (req, res) => {
  const { id, name, price, category_id } = req.body;

  const query = "CALL addOrEditProducts(?,?,?,?)";

  mysqlConnection.query(
    query,
    [id, name, price, category_id],
    (err, rows, fields) => {
      if (!err) {
        res.json({ status: "Producto añadido exitosamente!" });
      } else {
        console.log(err);
      }
    }
  );
});

router.put("/:id", (req, res) => {
  const { id } = req.params;
  const { name, price, category_id } = req.body;

  const query = "CALL addOrEditProducts(?,?,?,?)";
  mysqlConnection.query(
    query,
    [id, name, price, category_id],
    (err, rows, fields) => {
      if (!err) {
        res.json({ status: "Producto editado exitosamente!" });
      } else {
        console.log(err);
      }
    }
  );
});

router.delete("/:id", (req, res) => {
  const { id } = req.params;

  mysqlConnection.query(
    "DELETE FROM products WHERE id = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        res.json({ status: "Producto eliminado con exito!" });
      } else {
        console.log(err);
      }
    }
  );
});

module.exports = router;
