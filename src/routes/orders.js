const express = require("express");

const router = express.Router();

const mysqlConnection = require("../database/connection");





router.get("/", (req, res) => {
  mysqlConnection.query("SELECT * FROM orders", (err, rows, fields) => {
    if (err) {
      console.log(err);
    } else {
      res.json(rows);
    }
  });
});

router.get("/:id", (req, res) => {
  mysqlConnection.query(
    "SELECT * FROM orders WHERE id= ?",
    [req.params.id],
    (err, rows, fields) => {
      if (err) {
        console.log(err);
      } else {
        res.json(rows[0]);
      }
    }
  );
});

router.post("/", (req, res) => {
  const { id, store_id, staff_id, customer_id, status  } = req.body;

  const query = "CALL addOrEditOrders(?,?,?,?,?)";

  mysqlConnection.query(query, [id, store_id, staff_id, customer_id, status ], (err, rows, fields) => {
    if (!err) {
      res.json({ status: "Orden añadida exitosamente!" });
    } else {
      console.log(err);
    }
  });
});

router.put("/:id", (req, res) => {
  const { id } = req.params;
  const { store_id, staff_id, customer_id, status  } = req.body;

  const query = "CALL addOrEditOrders(?,?,?,?,?)";
  mysqlConnection.query(query, [id, store_id, staff_id, customer_id, status ], (err, rows, fields) => {
    if (!err) {
      res.json({ status: "Orden editada exitosamente!" });
    } else {
      console.log(err);
    }
  });
});

router.delete("/:id", (req, res) => {
  const { id } = req.params;

  mysqlConnection.query(
    "DELETE FROM orders WHERE id = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        res.json({ status: "Orden eliminada con exito!" });
      } else {
        console.log(err);
      }
    }
  );
});

module.exports = router;
