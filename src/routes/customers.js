const express = require("express");

const router = express.Router();

const mysqlConnection = require("../database/connection");





router.get("/", (req, res) => {
  mysqlConnection.query("SELECT * FROM customers", (err, rows, fields) => {
    if (err) {
      console.log(err);
    } else {
      res.json(rows);
    }
  });
});

router.get("/:id", (req, res) => {
  mysqlConnection.query(
    "SELECT * FROM customers WHERE id= ?",
    [req.params.id],
    (err, rows, fields) => {
      if (err) {
        console.log(err);
      } else {
        res.json(rows[0]);
      }
    }
  );
});

router.post("/", (req, res) => {
  const { id, name, email } = req.body;

  const query = "CALL addOrEditCustomers(?,?,?)";

  mysqlConnection.query(query, [id, name, email], (err, rows, fields) => {
    if (!err) {
      res.json({ status: "Cliente añadido exitosamente!" });
    } else {
      console.log(err);
    }
  });
});

router.put("/:id", (req, res) => {
  const { id } = req.params;
  const { name, email } = req.body;

  const query = "CALL addOrEditCustomers(?,?,?)";
  mysqlConnection.query(query, [id, name, email], (err, rows, fields) => {
    if (!err) {
      res.json({ status: "Cliente editado exitosamente!" });
    } else {
      console.log(err);
    }
  });
});

router.delete("/:id", (req, res) => {
  const { id } = req.params;

  mysqlConnection.query(
    "DELETE FROM customers WHERE id = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        res.json({ status: "Cliente eliminado con exito!" });
      } else {
        console.log(err);
      }
    }
  );
});

module.exports = router;
