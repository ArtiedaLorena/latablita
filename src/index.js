const express = require("express");
const app = express();
const cors = require("cors");

const morgan = require("morgan");

const stores = require("./routes/stores.js");
const customers = require("./routes/customers.js");
const orders = require("./routes/orders.js");
const staff = require("./routes/staff.js");
const products = require("./routes/products.js");

app.use(express.json());
app.use(morgan("tiny"));
app.use(cors());

app.use("/stores", stores);
app.use("/customers", customers);
app.use("/orders", orders);
app.use("/staff", staff);
app.use("/products", products);

app.use((req, res, next) => {
  res.send("<h1>Bienvenidos a la pagina de La Tablita</h1>");
});

app.use((req, res, next) => {
  res.status(404).send("<h1>ERROR 404: La pagina no ha sido encontrada</h1>");
});

app.listen(5000, () => {
  console.log("Servidor en el puerto 5000");
});
